﻿using UnityEngine;
using System.Collections;



public		class		Player				: MonoBehaviour {
	
	
	
	public							GameObject	playerObject			= null;		//player
	public							GameObject	bulletObject			= null;		//bullet
	
	public							Transform	bulletStartPosition		= null;		//bullet start position
	
	
	
	private		static readonly		float		MOVE_Z_FRONT			=  5.0f;	//move front
	private		static readonly		float		MOVE_Z_BACK				= -2.0f;	//move back
	
	private		static readonly		float		ROTATION_Y_KEY			= 360.0f;	//rotation by keyboard
	private		static readonly		float		ROTATION_Y_MOUSE		= 720.0f;	//rotation by mouse
	
	private							float		m_rotationY				= 0.0f;		//rotation value
	
	private							bool		m_mouseLockFlag			= true;		//flag of mouse lock
	
	

	private		void	Update() {
		
		// check target cleared
		if( Game.IsStageCleared()) {
			return;
		}
		
		//check mouse lock
		CheckMouseLock();
		
		//check player move
		CheckMove();
	}
	
	
	/*
	 *	Check Mouse Lock
	 */
	private		void	CheckMouseLock() {
		
		//Esc key
		if( Input.GetKeyDown( KeyCode.Escape)) {
			m_mouseLockFlag	= !m_mouseLockFlag;
		}
		
		if( m_mouseLockFlag) {
			Screen.lockCursor	= true;
			Cursor.visible	= false;
		} else {
			Screen.lockCursor	= false;
			Cursor.visible	= true;
		}
	}

	/*
	 *	Check Player move
	 */
	private		void	CheckMove() {
		
		//rotation
		{
			float	addRotationY	= 0.0f;

			// rotation by key
			if( Input.GetKey( KeyCode.Q)) {
				addRotationY		= -ROTATION_Y_KEY;
			} else
			if( Input.GetKey( KeyCode.E)) {
				addRotationY		=  ROTATION_Y_KEY;
			}
			
			//rotation by mouse
			if( m_mouseLockFlag) {
				addRotationY		+= (Input.GetAxis( "Mouse X")	*ROTATION_Y_MOUSE);
			}

			m_rotationY			+= (addRotationY	*Time.deltaTime);
			transform.rotation	= Quaternion.Euler( 0, m_rotationY, 0);	
		}

		Vector3	addPosition	= Vector3.zero;
		//move
		{
			Vector3	vecInput		= new Vector3( 0f, 0, Input.GetAxisRaw( "Vertical"));

			if( vecInput.z > 0) { //front
				addPosition.z		= MOVE_Z_FRONT;
			} else if( vecInput.z < 0) { //back
				addPosition.z		= MOVE_Z_BACK;
			}

			transform.position	+= ((transform.rotation	 	*addPosition)		*Time.deltaTime);
		}
		
		//shoot bullet
		bool	shootFlag;
		{
			if( Input.GetButtonDown( "Fire1")) {
				shootFlag	= true;

				if( null!=bulletStartPosition) {
					Vector3 vecBulletPos	= bulletStartPosition.position;
					vecBulletPos			+= (transform.rotation	*Vector3.forward);
					vecBulletPos.y			= 1.0f;

					Instantiate( bulletObject, vecBulletPos, transform.rotation);
				}
			} else {
				shootFlag	= false;
			}
		}
		

		{
			Animator	animator	= playerObject.GetComponent<Animator>();

			animator.SetFloat(	"SpeedZ",	addPosition.z);
			animator.SetBool(	"Shoot",	shootFlag);	
		}
	}
	
	
	
	
}
